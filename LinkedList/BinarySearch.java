/**Given a sorted (in ascending order) integer array nums of n elements and a target value, write a function to search target in nums. If target exists, then return its index, otherwise return -1

*/


class BinarySearch {
    public int search(int[] nums, int target) {
        int left =0,right = nums.length-1,pivot = 0 ;
        
        while (left <= right) {
            pivot =left +  (right-left)/2;
             if (nums[pivot] == target) return pivot;
            if (nums[pivot] > target) right = pivot - 1;
            else left = pivot-1;
        }
        
        return  -1;
    }
}
