/**

Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions

 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class PartitionList {
    public ListNode partition(ListNode head, int x) {
        ListNode beforehead = new ListNode(0);
        ListNode afterhead  = new ListNode(0);
        ListNode before = beforehead;
        ListNode after = afterhead;
        
        while (head !=null) {
            if (head.val <x) {
                before.next = head;
                before = before.next;
            }
            else {
                after.next = head;
                after = after.next;
            }
            head = head.next;
        }
        after.next = null;
        before.next = afterhead.next;
        return beforehead.next;
    }
}
