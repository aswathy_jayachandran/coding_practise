import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


/* Given a m * n matrix mat of integers, sort it diagonally in ascending order from the top-left to the bottom-right then return the sorted array */

public class diagonalsort {
	
	public static void diagonalsortarray(int [][] matrix,int m,int n) {
		Map <Integer ,List<Integer>> sorting = new HashMap<>();
		
	    for(int i=0;i<m;i++) {
	    	for(int j=0;j<n;j++) {
	    		int diff = i-j;
	    	 if(!sorting.containsKey(diff))	{	
	    		sorting.put(diff, new ArrayList<>());
	    	 }
	    	 sorting.get(diff).add(matrix[i][j]);
	    	}
	    }
	    System.out.println("Map before sorting :" +sorting);
	   for(java.util.Map.Entry<Integer, List<Integer>> sorted : sorting.entrySet()) {
		   
		   Collections.sort(sorted.getValue());  
	   }
	   System.out.println("SortedMAP : " +sorting);
	   for(int i =0;i<m ;i++) {
		   for(int j=0;j<n;j++) {
			   int diff =i-j;
			  matrix[i][j]=sorting.get(diff).get(0);
			  sorting.get(diff).remove(0);
		   }
	   }
	   
	   System.out.println("Array after Diagonal Sort:");
	  printArray(matrix,m,n);
	}
	
	public static void printArray(int[][] matrix,int m, int n) {
	 
		for(int i=0;i<m;i++) {
			   for(int j=0;j<n;j++) {
				   System.out.print(matrix[i][j]);
			   }
			   System.out.println("");
		   }
    }
	
	public static void main(String [] args) {
		int m,n,i,j;
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter the number of rows");
		m= obj.nextInt();
		System.out.println("Enter number of columns");
		n=obj.nextInt();
		int [][] matrix =new int[m][n];
		System.out.println("Enter elements of an array");
		for(i=0;i<m;i++) {
			for(j=0;j<n;j++) {
				matrix[i][j]= obj.nextInt();
			}
		}
		
		System.out.println(" array");
		printArray(matrix,m,n);
		diagonalsortarray(matrix,m,n);
	}
}

