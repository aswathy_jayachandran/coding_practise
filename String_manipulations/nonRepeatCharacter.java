import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
public class nonRepeatCharacter {
 private static void printNonDuplicate(String checkDuplicate) {

		char[] singleChars = checkDuplicate.toCharArray();

		Map<Character, Integer> charMap = new HashMap<>();

		for (char singleChar : singleChars) {
			if (!charMap.isEmpty() && charMap.containsKey(singleChar)) {
				charMap.put(singleChar, charMap.get(singleChar) + 1);
				
			} else
				charMap.put(singleChar, 1);
		}
		
		if (charMap.containsValue(1)) {
			Optional<Character> o = charMap.entrySet()
                    .stream()
                    .filter( e -> e.getValue() == 1)
                    .map(Map.Entry::getKey)
                    .findFirst();
			
			System.out.print("First Non repeatedCharacter  -  " +o.get());
		} else
			System.out.print(" No Non repeatedCharecters in : "+checkDuplicate);

	}

	public static void main(String args[]) {
		try (Scanner myObj = new Scanner(System.in)) {
			System.out.println("Enter a String");

			String checkDuplicate = myObj.nextLine();
			
			printNonDuplicate(checkDuplicate);
		}

	}
}
