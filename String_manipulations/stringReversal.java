import java.util.Scanner;
import java.util.Stack;

public class stringReversal {
	public static void main(String[] args) {
		  Scanner obj = new Scanner(System.in);
		  System.out.println("Enter a String");
		  String sample = obj.nextLine();
		  System.out.println("Given String :" +sample);
		  System.out.print("String in reverse order :" );
		  char [] chars = sample.toCharArray();
		  Stack <Character> charstack  = new Stack<Character>();
		  for (char c : chars) {
			  charstack.push(c);
		  }
		  while(!charstack.isEmpty()) {
			  System.out.print(charstack.pop());
		  }
		 }

}
