
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DuplicateString{

     	private static void printDuplicate(String checkDuplicate) {

		char[] singleChars = checkDuplicate.toCharArray();
		Boolean duplicates = false;

		Map<Character, Integer> charMap = new HashMap<>();

		for (char singleChar : singleChars) {
			if (!charMap.isEmpty() && charMap.containsKey(singleChar)) {
				charMap.put(singleChar, charMap.get(singleChar) + 1);
				duplicates = true;
			} else
				charMap.put(singleChar, 1);
		}
		
		if (duplicates) {
			System.out.print("Duplicates characters in given String  - ");
			charMap.forEach((key, value) -> {
				if (value >1) {
				System.out.print((key +" "));	
				}
			});
		} else
			System.out.print(" No duplicate characters available in : "+checkDuplicate);

	}

	public static void main(String args[]) {
		try (Scanner myObj = new Scanner(System.in)) {
			System.out.println("Enter a String");

			String checkDuplicate = myObj.nextLine();
			
			printDuplicate(checkDuplicate);
		}

	}
}

